package programa;

import java.util.Scanner;

import clases.GestorClases;


/**
 * Clase Programa.
 * @author Hello there
 * 
 */
public class Programa {

	/**
	 * Metodo main.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		
	GestorClases gestor = new GestorClases();
	gestor.altaMaestro("Yoda", 1, "verde", "Jedi", "Ataru", "Pulso de fuerza");
	gestor.altaMaestro("Mace window", 1, "morado", "Jedi", "Juyo / Vaapad", "Salto de fuerza");
	gestor.altaMaestro("Grievous", 4, "rojo, verde, azul, amarillo", "Sith", "Juyo / Vaapad", "Todocamino");
	
	
	gestor.altaAprendiz("Darth Maul", "zabrak dathomiriano", "Acrobata", 500);
	gestor.altaAprendiz("Darth Revan", "Humano", "Espia", 1250);
	gestor.altaAprendiz("manolo", "zabrak dathomiriano", "ladron", 8);
	
	

	Scanner input = new Scanner(System.in);
	
	boolean eleccion = true;
	
	do {
		
		System.out.println("**************************************");
		System.out.println("�Que desea hacer?");
		System.out.println("1. Listar Maestro");
		System.out.println("2. Buscar Maestro");
		System.out.println("3. Eliminar maestro");
		System.out.println("4. Listar Aprendiz");
		System.out.println("5. Buscar Aprendiz");
		System.out.println("6. Eliminar Aprendiz");
		System.out.println("7. Listar Aprendiz por raza");
		System.out.println("8. Asignar Maestro en Aprendiz");
		System.out.println("9. Cambiar Maestro");
		System.out.println("10. Eliminar Aprendices desmaestrados");
		System.out.println("11. SALIR");
		System.out.println("**************************************");
		
		
		int menu = 0;
		String dato = "";
		while (eleccion) {
			dato = input.nextLine();
			if (dato.length() == 0) {
				System.out.println("Introduzca una opcion entre las posibles");
				eleccion = true;
			} else {
				
				for(int i = 0; i < dato.length(); i++) {
					if(dato.charAt(i) >= '0' && dato.charAt(i) <= '9') {
						eleccion = false;
					}else {
						eleccion = true;
						break;
					}
				}
				
			}
		}
		menu = Integer.parseInt(dato);
		
		switch(menu) {
		
		case 1: // Listar Maestro
			System.out.println();
			
			System.out.println("listamos Maestros");
			gestor.listarMaestros();
			
			System.out.println();
			eleccion = true;
			break;			
			//*************************************************
		case 2: // Buscar Maestro
			System.out.println();
			
			System.out.println("Buscamos Maestro entre los posibles");
			System.out.println("Yoda, Mace window, Grievous");
			String maestrillo = input.nextLine();
			if(maestrillo.equalsIgnoreCase("Yoda")) {
			System.out.println();	
			System.out.println(gestor.buscarMaestro("Yoda"));	
			
			}
			if (maestrillo.equalsIgnoreCase("Mace window")) {
				System.out.println();
				System.out.println(gestor.buscarMaestro("Mace window"));	
				
			} 
			if (maestrillo.equalsIgnoreCase("Grievous")) {
				System.out.println();
				System.out.println(gestor.buscarMaestro("Grievous"));	
			}
			
			
			System.out.println();
			eleccion = true;
			break;			
			//*************************************************
		case 3: // Eliminar maestro
			System.out.println();
			
			System.out.println("Elegimos Maestro entre los posibles para eliminar");
			System.out.println("Yoda, Mace window, Grievous");
			gestor.listarMaestros();
			String eliminacion = input.nextLine();
			if(eliminacion.equalsIgnoreCase("Yoda")) {
			System.out.println();	
			gestor.eliminarMaestro("Yoda");
			System.out.println();
			gestor.listarMaestros();
			}
			if (eliminacion.equalsIgnoreCase("Mace window")) {
				System.out.println();
				gestor.eliminarMaestro("Mace window");
				System.out.println();
				gestor.listarMaestros();
			} 
			if (eliminacion.equalsIgnoreCase("Grievous")) {
				System.out.println();
				gestor.eliminarMaestro("Grievous");
				System.out.println();
				gestor.listarMaestros();
			}
			
			System.out.println();
			eleccion = true;
			break;
			//*************************************************
		case 4: // Listar Aprendiz
			System.out.println();
			
			System.out.println("listamos Aprendices");
			gestor.listarAprendices();
			
			System.out.println();
			eleccion = true;
			break;
			//*************************************************
		case 5: // Buscar Aprendiz
			System.out.println();
			
			System.out.println("Buscamos Aprendiz");
			System.out.println("Darth Maul, Darth Revan, manolo");
			String aprendicillo = input.nextLine();
			if(aprendicillo.equalsIgnoreCase("Darth Maul")) {
			System.out.println();	
			System.out.println(gestor.buscarMaestro("Darth Maul"));	
			
			}
			if (aprendicillo.equalsIgnoreCase("Darth Revan")) {
				System.out.println();
				System.out.println(gestor.buscarMaestro("Darth Revan"));	
				
			} 
			if (aprendicillo.equalsIgnoreCase("manolo")) {
				System.out.println();
				System.out.println(gestor.buscarMaestro("manolo"));	
			}
			
			System.out.println();
			eleccion = true;
			break;
			//*************************************************
		case 6: // Eliminar Aprendiz
			System.out.println();
			
			System.out.println("Eliminamos aprendiz y listamos");
			gestor.listarAprendices();
			String eliminacion2 = input.nextLine();
			if(eliminacion2.equalsIgnoreCase("Darth Maul")) {
			System.out.println();	
			gestor.eliminarAprendiz("Darth Maul");
			System.out.println();
			gestor.listarAprendices();
			}
			if (eliminacion2.equalsIgnoreCase("Darth Revan")) {
				System.out.println();
				gestor.eliminarAprendiz("Darth Revan");
				System.out.println();
				gestor.listarAprendices();
			} 
			if (eliminacion2.equalsIgnoreCase("manolo")) {
				System.out.println();
				gestor.eliminarAprendiz("manolo");
				System.out.println();
				gestor.listarAprendices();
			}
			
			System.out.println();
			eleccion = true;
			break;
			//*************************************************
		case 7: // Listar Aprendiz por raza
			System.out.println();
			
			System.out.println("Listamos aprendiz por Raza");
			gestor.listarAprendizPorRaza("zabrak dathomiriano");
			
			System.out.println();
			eleccion = true;
			break;
			//*************************************************
		case 8: // Asignamos Maestro en Aprendiz y listamos
			System.out.println();
			
			System.out.println("Asignamos aprendices a maestros");
			gestor.asignarUnMaestro("Yoda", "manolo");
			
			System.out.println();
			
			System.out.println("Y listamos aprendices de maestros");
			gestor.listarAprendicesDeMaestro("Yoda");
			
			System.out.println();
			eleccion = true;
			break;
			//*************************************************
		case 9: // cambiamos Maestro
			System.out.println();
			
			System.out.println("Cambiamos el maestro");
			gestor.cambiarMaestro("Yoda", "Mace window", "manolo");
			gestor.listarAprendicesDeMaestro("Mace window");
			
			System.out.println();
			eleccion = true;
			break;
			//*************************************************
		case 10: // Eliminar Aprendices desmaestrados
			System.out.println();
			
			System.out.println("Eliminamos a aquellos sin maestro");
			gestor.reajusteMaestros();
			gestor.listarAprendices();
			
			System.out.println();
			eleccion = true;
			break;
			//*************************************************
		case 11: // SALIR
			eleccion = false;
			break;
			
		default:
			System.out.println("Elija una opcion contemplada en el menu");
		
		}
		
		
		
	} while (eleccion);
	
	input.close();
	}

}

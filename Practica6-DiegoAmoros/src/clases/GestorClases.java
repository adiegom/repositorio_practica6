package clases;

import java.util.ArrayList;
import java.util.Iterator;


/**
 *Clase GestorClases.
 *Clase gestora de metodos
 *
 */
public class GestorClases {
	
	/** lista de maestros. */
	private ArrayList<Maestro>listaDeMaestros;
	
	/** lista de aprendices. */
	private ArrayList<Aprendiz>listaDeAprendices;
	
	/**
	 * Instanciacion clases gestoras.
	 */
	public GestorClases() {
		listaDeMaestros = new ArrayList<Maestro>();
		listaDeAprendices = new ArrayList<Aprendiz>();
	}
	
	/**
	 * Alta maestro.
	 *
	 * @param nombre. Te permite a�adir un nombre.
	 * @param numeroSablesLaser. Te permite a�adir un numero Sables Laser.
	 * @param coloSableLaser. Te permite a�adir un colo Sable Laser.
	 * @param faccion. Te permite a�adir un faccion.
	 * @param estiloDeCombate. Te permite a�adir un estilo De Combate.
	 * @param poder. Te permite a�adir un poder.
	 */
	public void altaMaestro(String nombre, int numeroSablesLaser, String coloSableLaser, 
			String faccion, String estiloDeCombate, String poder) {
		
		if (!existeMaestro(nombre)) {
			Maestro nuevoMaestro = new Maestro(nombre, numeroSablesLaser, coloSableLaser, faccion, estiloDeCombate, poder);
			listaDeMaestros.add(nuevoMaestro);
		} else {
			System.out.println("Ese Maestro sigue vivo, prueba con otro.");
		}
		
	}
	
	/**
	 * Existe maestro. Compreuba la existencia de un maestro al intentar a�adir uno nuevo.
	 *
	 * @param nombre. 
	 * @return true, si se cumple.
	 */
	public boolean existeMaestro (String nombre) {
		for (Maestro maestro: listaDeMaestros) {
			if (maestro != null && maestro.getNombre().equalsIgnoreCase(nombre)) {
				return true;
				
			}
		}
		return false;
	}
	
	/**
	 * Listar maestros.
	 */
	public void listarMaestros() {
		for (Maestro maestro: listaDeMaestros) {
			if (maestro != null) {
				System.out.println(maestro);
			}
		}
	}
	
	/**
	 * Buscar maestro. Busca un maestro.
	 *
	 * @param nombre.
	 * @return maestro.
	 */
	public Maestro buscarMaestro (String nombre) {
		for (Maestro maestro: listaDeMaestros) {
			if(maestro != null && maestro.getNombre().equalsIgnoreCase(nombre)) {
				return maestro;
			}
		}
		return null;
	}
	
	/**
	 * Eliminar maestro.
	 *
	 * @param nombre.
	 */
	public void eliminarMaestro(String nombre) {
		Iterator<Maestro>iteratorMaster = listaDeMaestros.iterator();
		
		while (iteratorMaster.hasNext()) {
			Maestro master = iteratorMaster.next();
			if(master.getNombre().equalsIgnoreCase(nombre)) {
				iteratorMaster.remove();
			}
		}
	}
	
//************************************************************************************************************************
	
	/**
 * Alta aprendiz.
 *
 * @param nombre. Te permite a�adir un nombre.
 * @param raza. Te permite a�adir una raza.
 * @param habilidad. Te permite a�adir una habilidad.
 * @param misionesCumplidas. Te permite a�adir las misiones Cumplidas.
 */
public void altaAprendiz(String nombre, String raza, 
			String habilidad, int misionesCumplidas) {
		
		if (!existeAprendiz(nombre)) {
			Aprendiz nuevoAprendiz = new Aprendiz(nombre, raza, habilidad, misionesCumplidas);
			listaDeAprendices.add(nuevoAprendiz);
		} else {
			System.out.println("Ese Aprendiz no es nuevo, sigue entre nuestras filas.");
		}
		
	}
	
	/**
	 * Existe aprendiz. Compreuba la existencia de un aprendiz al intentar a�adir uno nuevo.
	 *
	 * @param nombre the nombre.
	 * @return true, si se cumpre.
	 */
	public boolean existeAprendiz (String nombre) {
		for (Aprendiz aprendiz: listaDeAprendices) {
			if (aprendiz != null && aprendiz.getNombre().equalsIgnoreCase(nombre)) {
				return true;
				
			}
		}
		return false;
	}
	
	/**
	 * Listar aprendices.
	 */
	public void listarAprendices() {
		for (Aprendiz aprendiz: listaDeAprendices) {
			if (aprendiz != null) {
				System.out.println(aprendiz);
			}
		}
	}
	
	/**
	 * Eliminar aprendiz.
	 *
	 * @param nombre the nombre
	 */
	public void eliminarAprendiz(String nombre) {
		Iterator<Aprendiz>iteratorAprendiz = listaDeAprendices.iterator();
		
		while (iteratorAprendiz.hasNext()) {
			Aprendiz aprendiz = iteratorAprendiz.next();
			if(aprendiz.getNombre().equalsIgnoreCase(nombre)) {
				iteratorAprendiz.remove();
			}
		}
	}
	
	/**
	 * Buscar aprendiz.
	 *
	 * @param nombre the nombre
	 * @return the aprendiz
	 */
	public Aprendiz buscarAprendiz (String nombre) {
		for (Aprendiz aprendiz: listaDeAprendices) {
			if(aprendiz != null && aprendiz.getNombre().equalsIgnoreCase(nombre)) {
				return aprendiz;
			}
		}
		return null;
	}
	
	/**
	 * Listar aprendiz por raza.
	 *
	 * @param raza.
	 */
	public void listarAprendizPorRaza (String raza) {
		for (Aprendiz aprendiz: listaDeAprendices) {
			if (aprendiz.getRaza().equalsIgnoreCase(raza)) {
				System.out.println(aprendiz);
			}
		}
	}
	
	
	/**
	 * Listar aprendices de maestro.
	 *
	 * @param nombre.
	 */
	public void listarAprendicesDeMaestro (String nombre) {
		for (Aprendiz aprendiz: listaDeAprendices) {
			if (aprendiz.getMaestro() != null && 			aprendiz.getMaestro().getNombre().equalsIgnoreCase(nombre));
			System.out.println(aprendiz);
			System.out.println(aprendiz.getMaestro());
		}
	}
	

	/**
	 * Asignar un maestro.
	 *
	 * @param nombre.
	 * @param nombreAprendiz.
	 */
	public void asignarUnMaestro(String nombre, String nombreAprendiz) {
		if (buscarMaestro(nombre) != null && buscarAprendiz(nombreAprendiz) != null) {
			Maestro maestro = buscarMaestro(nombre);
			Aprendiz aprendiz = buscarAprendiz(nombreAprendiz);
			aprendiz.setMaestro(maestro);
			System.out.println(maestro.getNombre());
		} 
	}
	
	/**
	 * Cambiar maestro.
	 *
	 * @param nombre.
	 * @param nombre2.
	 * @param nombreAprendiz.
	 */
	public void cambiarMaestro(String nombre, String nombre2, String nombreAprendiz) {
		for (int i = 0; i < listaDeAprendices.size(); i++) {
			Maestro maestroActual = listaDeMaestros.get(i);
			if (buscarMaestro(nombre) != null) {
				Maestro maestro = buscarMaestro(nombre2);
				Aprendiz aprendiz = buscarAprendiz(nombreAprendiz);
				aprendiz.setMaestro(maestro);
			}
		}
	}
	
	
	/**
	 * Reajuste maestros. Elimina aquellos aprendices que carecen de maestro.
	 */
	public void reajusteMaestros () {
		Iterator<Aprendiz> iteradorDeAprendices = listaDeAprendices.iterator();
		
		while (iteradorDeAprendices.hasNext()) {
			Aprendiz aprendizDesmaestrado = iteradorDeAprendices.next();
			boolean desmaestrado = true;
			for(Aprendiz aprendiz: listaDeAprendices) {
				if (aprendiz.getMaestro() != null && aprendiz.equals(aprendizDesmaestrado)) {
					desmaestrado = false;
				}
			}
			if (desmaestrado) {
				iteradorDeAprendices.remove();
			}
		}
	}
	
}

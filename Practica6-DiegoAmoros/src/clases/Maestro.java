package clases;


/**
 * Clase Maestro
 * Clase1.
 */
public class Maestro {

	/** nombre. */
	private String nombre;
	
	/** numero sables laser. */
	private int numeroSablesLaser;
	
	/** colo sable laser. */
	private String coloSableLaser;
	
	/** faccion. */
	private String faccion;
	
	/** estilo de combate. */
	private String estiloDeCombate;
	
	/** poder. */
	private String poder;

	/**
	 * Instanciacion maestro.
	 */
	public Maestro() {
		super();
		this.nombre = "-";
		this.numeroSablesLaser = 0;
		this.coloSableLaser = "-";
		this.faccion = "-";
		this.estiloDeCombate = "-";
		this.poder = "-";
	}

	/**
	 * Instantiates a new maestro.
	 *
	 * @param nombre. Te permite a�adir un nombre.
	 * @param numeroSablesLaser. Te permite a�adir un numero Sables Laser.
	 * @param coloSableLaser. Te permite a�adir un colo Sable Laser.
	 * @param faccion. Te permite a�adir un faccion.
	 * @param estiloDeCombate. Te permite a�adir un estilo De Combate.
	 * @param poder. Te permite a�adir un poder.
	 */
	public Maestro(String nombre, int numeroSablesLaser, String coloSableLaser, String faccion, String estiloDeCombate,
			String poder) {
		super();
		this.nombre = nombre;
		this.numeroSablesLaser = numeroSablesLaser;
		this.coloSableLaser = coloSableLaser;
		this.faccion = faccion;
		this.estiloDeCombate = estiloDeCombate;
		this.poder = poder;

	}

	/**
	 * Gets the nombre.
	 *
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Sets the nombre.
	 *
	 * @param nombre the new nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Gets the numero sables laser.
	 *
	 * @return the numero sables laser
	 */
	public int getNumeroSablesLaser() {
		return numeroSablesLaser;
	}

	/**
	 * Sets the numero sables laser.
	 *
	 * @param numeroSablesLaser the new numero sables laser
	 */
	public void setNumeroSablesLaser(int numeroSablesLaser) {
		this.numeroSablesLaser = numeroSablesLaser;
	}

	/**
	 * Gets the colo sable laser.
	 *
	 * @return the colo sable laser
	 */
	public String getColoSableLaser() {
		return coloSableLaser;
	}

	/**
	 * Sets the colo sable laser.
	 *
	 * @param coloSableLaser the new colo sable laser
	 */
	public void setColoSableLaser(String coloSableLaser) {
		this.coloSableLaser = coloSableLaser;
	}

	/**
	 * Gets the faccion.
	 *
	 * @return the faccion
	 */
	public String getFaccion() {
		return faccion;
	}

	/**
	 * Sets the faccion.
	 *
	 * @param faccion the new faccion
	 */
	public void setFaccion(String faccion) {
		this.faccion = faccion;
	}

	/**
	 * Gets the estilo de combate.
	 *
	 * @return the estilo de combate
	 */
	public String getEstiloDeCombate() {
		return estiloDeCombate;
	}

	/**
	 * Sets the estilo de combate.
	 *
	 * @param estiloDeCombate the new estilo de combate
	 */
	public void setEstiloDeCombate(String estiloDeCombate) {
		this.estiloDeCombate = estiloDeCombate;
	}

	/**
	 * Gets the poder.
	 *
	 * @return the poder
	 */
	public String getPoder() {
		return poder;
	}

	/**
	 * Sets the poder.
	 *
	 * @param poder the new poder
	 */
	public void setPoder(String poder) {
		this.poder = poder;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Maestro [nombre: " + nombre + ", numeroSablesLaser: " + numeroSablesLaser + ", coloSableLaser: "
				+ coloSableLaser + ", faccion: " + faccion + ", estiloDeCombate: " + estiloDeCombate + ", poder: "
				+ poder + "]";
	}
}

package clases;

/**
 * Clase Aprendiz.
 * Clase 2
 */
public class Aprendiz {

	/** nombre. */
	private String nombre;
	
	/** raza. */
	private String raza;
	
	/** habilidad. */
	private String habilidad;
	
	/** misiones cumplidas. */
	private int misionesCumplidas;
	
	/** maestro. */
	private Maestro maestro;

	/**
	 * Instanciacion aprendiz.
	 */
	public Aprendiz() {
		super();
		this.nombre = "-";
		this.raza = "-";
		this.habilidad = "-";
		this.misionesCumplidas = 0;
		
	}

	/**
	 * Instantiates a new aprendiz.
	 *
	 * @param nombre. Te permite a�adir un nombre.
	 * @param raza. Te permite a�adir un raza.
	 * @param habilidad. Te permite a�adir un habilidad.
	 * @param misionesCumplidas. Te permite a�adir un misiones Cumplidas.
	 */
	public Aprendiz(String nombre, String raza, String habilidad, int misionesCumplidas) {
		super();
		this.nombre = nombre;
		this.raza = raza;
		this.habilidad = habilidad;
		this.misionesCumplidas = misionesCumplidas;
		

	}

	/**
	 * Gets the nombre.
	 *
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Sets the nombre.
	 *
	 * @param nombre the new nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Gets the raza.
	 *
	 * @return the raza
	 */
	public String getRaza() {
		return raza;
	}

	/**
	 * Sets the raza.
	 *
	 * @param raza the new raza
	 */
	public void setRaza(String raza) {
		this.raza = raza;
	}

	/**
	 * Gets the habilidad.
	 *
	 * @return the habilidad
	 */
	public String getHabilidad() {
		return habilidad;
	}

	/**
	 * Sets the habilidad.
	 *
	 * @param habilidad the new habilidad
	 */
	public void setHabilidad(String habilidad) {
		this.habilidad = habilidad;
	}

	/**
	 * Gets the misiones cumplidas.
	 *
	 * @return the misiones cumplidas
	 */
	public int getMisionesCumplidas() {
		return misionesCumplidas;
	}

	/**
	 * Sets the misiones cumplidas.
	 *
	 * @param misionesCumplidas the new misiones cumplidas
	 */
	public void setMisionesCumplidas(int misionesCumplidas) {
		this.misionesCumplidas = misionesCumplidas;
	}

	/**
	 * Gets the maestro.
	 *
	 * @return the maestro
	 */
	public Maestro getMaestro() {
		return maestro;
	}

	/**
	 * Sets the maestro.
	 *
	 * @param maestro the new maestro
	 */
	public void setMaestro(Maestro maestro) {
		this.maestro = maestro;
	}

	
	
	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Aprendiz [nombre: " + nombre + ", raza: " + raza + ", habilidad: " + habilidad + ", misionesCumplidas: "
				+ misionesCumplidas + ", maestro: " + maestro + "]";
	}





}
